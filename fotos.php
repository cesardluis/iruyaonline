<?php
header('Content-Type: text/html; charset=UTF-8');
include_once "libs/tmpl.class.php";
include_once "libs/mysql.php";

$mysql = new   MysqlDB('192.168.0.61', 'user_online', 'cl.216-user', 'bd_iruyaonline');

if (! isset($_GET['gal'])){
	$html = '<h2 class="title">Galerias de Fotos de Iruya</h2>';
	$galerias = $mysql->get('galerias');
	foreach ($galerias as $galeria) {
		$html .='<div class="columns small-12 medium-4 large-3 text-center">
			<a href="http://www.iruyaonline.com/fotos-'.$galeria['dir'].'.html" class="th radius"><img src="http://www.iruyaonline.com/fm-fotos/'.$galeria['dir'].'/'.$galeria['imagen'].'"></a>
			<div class="titulo">'.$galeria['titulo'].'</div>
			</div>';
	}
	$data = array(
		'title'  => 'Fotos de Iruya',
		'description'  => 'Gran galeria de fotos de Iruya.',
		'banner' => '',
		'content'  => $html,
		'url' => $_SERVER['REQUEST_URI'],
		"host" => "http://www.iruyaonline.com/"
	);
}else{
	$mysql->where('dir', $_GET['gal']);
	$galeria = $mysql->get('galerias', 1);

	$mysql->where('galeria', $galeria['id']);
	$fotos = $mysql->get('galerias_foto');

	$html = '<div class="columns small-12"><h2 class="title">'.$galeria['titulo'].'</h2>';
	$html .= ' <ul class="clearing-thumbs small-block-grid-2 medium-block-grid-4 large-block-grid-5" data-clearing>';
	foreach ($fotos as $foto) {
		$html .='<li>
			<a href="http://www.iruyaonline.com/fg-fotos/'.$galeria['dir'].'/'.$foto['foto'].'"  class="th radius">
				<img data-caption="'.$foto['titulo'].'" src="http://www.iruyaonline.com/fm-fotos/'.$galeria['dir'].'/'.$foto['foto'].'">
			</a></li>';
	}
	$html .= '</ul></div>';

	$data = array(
		'title'  => $galeria['titulo'],
		'description'  => $galeria['descripcion'],
		'banner' => '',
		'content'  => $html,
		'url' => $_SERVER['REQUEST_URI'],
		"host" => "http://www.iruyaonline.com/"
	);

}


$html = new tmpl('template/iruyaonline.tpl', $data);
$html->display();
//$path = 'fotos/'.$_GET['gal'].'/';