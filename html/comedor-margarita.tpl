<title>Comedor Margarita</title>
<description>Provee a sus clientes de un ambiente confortable y acogedor con  una atención personalizada y cordial. Donde se brinda una gran variedad de comidas tanto regionales como minutas.</description>
<content>
	<section class="columns small-12 medium-12 large-12">
		<h1>Comedor Margarita</h1>
		<ul class="small-block-grid-2 medium-block-grid-2 large-block-grid-3 clearing-thumbs" data-clearing>
			<li><a class="th" title="Local Margarita" href="//d3bgb83gsu3958.cloudfront.net/imagenes/margarita-local3.jpg"><img src="m-margarita-local3.jpg" alt="Local Margarita" /></a></li>
			<li><a class="th" title="Local Margarita" href="//d3bgb83gsu3958.cloudfront.net/imagenes/margarita-local.jpg"><img src="m-margarita-local.jpg" alt="Local Margarita" /></a></li>
			<li><a class="th" title="Local Margarita" href="//d3bgb83gsu3958.cloudfront.net/imagenes/margarita-local2.jpg"><img src="m-margarita-local2.jpg" alt="Local Margarita" /></a></li>
		</ul>
		<p>Un emprendimiento Familiar, que funciona en la localidad de Iruya desde el año 2004.</p>
		<p>Provee a sus clientes de un ambiente confortable y acogedor con una atención personalizada y cordial. Donde se brinda una gran variedad de comidas tanto regionales como minutas.</p>
		<h3>Servicios</h3>
		<p>El comedor funciona de Lunes a Domingo de 08:00 a 15:00 y de 19:00 a 24:00.<br /> <em>&gt;&gt; Los horarios de atención mencionados para el día Domingo corresponden sólo a temporada alta Invierno - Verano&lt;&lt;</em><br /> Este comedor tiene capacidad para 40 (cuarenta) personas, distribuidas en dos salones equipados con Televisor y DVD para hacer del momento lo más ameno posible.</p>
		<h3>Menu </h3>
		<p>Todos los días de atención COMEDOR &ldquo;MARGARITA&rdquo; ofrece 2 (dos) Menús diferentes, que incluyen 1 Plato Principal, Sopa y Postre. Estos menús representan un costo inferior que el menú a la carta.</p>
		<h4>Comidas </h4>
		<p>Entre la diversidad de comidas que se ofrecen en COMEDOR &ldquo;MARGARITA&rdquo; los comensales podrán degustar de:</p>
		<ul class="small-block-grid-2 medium-block-grid-2 large-block-grid-4 clearing-thumbs" data-clearing>
			<li><a class="th" title="Locro Regional" href="//d3bgb83gsu3958.cloudfront.net/imagenes/margarita-locro.jpg"><img src="m-margarita-locro.jpg" alt="margarita-locro" /></a></li>
			<li><a class="th" title="Estofado de Cordero" href="//d3bgb83gsu3958.cloudfront.net/imagenes/margarita-estofado.jpg"><img src="m-margarita-estofado.jpg" alt="margarita-estofado" /></a></li>
			<li><a class="th" title="Tortilla de Quinoa" href="//d3bgb83gsu3958.cloudfront.net/imagenes/margarita-torilla-de-quinoa.jpg"><img src="m-margarita-torilla-de-quinoa.jpg" alt="margarita-torilla-de-quinoa" /></a></li>
			<li><a class="th" title="Milanesa de Llama" href="//d3bgb83gsu3958.cloudfront.net/imagenes/margarita-mila.jpg"><img src="m-margarita-mila.jpg" alt="margarita-milanesa" /></a></li>
		</ul>
		<ul>
			<li>Variedad de Empanadas de carne (vaca o llama), de pollo y/o de queso.</li>
			<li>Asados (cordero, cerdo, llama)</li>
			<li>Variedad en Guisos elaborados con productos regionales. </li>
			<li>Locro.</li>
			<li>Tamales </li>
			<li>Comidas y postre elaborado con QUINOA</li>
			<li>Guarniciones: papas andinas, ocas y ensaladas.</li>
			<li>Pastas y minutas. Sándwiches. Pizzas </li>
		</ul>
		<h4>Bebidas.</h4>
		<ul>
			<li><strong>Bebidas Frías:</strong><br />GASEOSAS de primera línea, clásicas y light.<br /> Vinos finos.<br /> Cerveza.<br /> Agua mineral y saborizada.</li>
			<li> <strong>Bebidas calientes:</strong><br /> Te<br /> Café <br /> Infusiones </li>
		</ul>
		<div class="panel ">
			<p><strong>Reservas y Consultas</strong><br /> Para acceder a los servicios de COMEDOR &ldquo;MARGARITA&rdquo; puede comunicarse a los siguientes números:<br /> (0387) 155433720<br /> O correos electrónicos: valrolamas@gmail.com</p>
		</div>
	</section>
</content>
