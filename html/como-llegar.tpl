<title>Como llegar a Iruya</title>
<description>En Vehiculo propio o en colectivo existe una única forma de hacerlo, pasando la provincia de Jujuy, recorriendo la ruta Nacional Nº 9, a 26 km. De la capital de Humahuaca está el cruce de rutas, donde se lee Iruya 54 Km</description>
<content>
	<section class="columns small-12 medium-8 large-8">
		<h1 class="title">Cómo llegar a Iruya</h1>
		<div class="panel">
			<p><strong>Tiempo que se tarda desde  Iruya a Humahuaca:</strong></p>
			<ul><li>En Colectivo: de 3 hrs a 3 &frac12; hrs ;</li><li>En Vehículo Propio: de 2 &frac12; hrs a 4, dependiendo  de cada uno. </li></ul>
			<p><strong>Frecuencia de los  colectivos desde Humahuaca a Iruya. </strong></p>
			<ul><li><strong>Transporte Iruya</strong>: desde Humahuaca salen colectivos a las 8.20, 10.30 y 16.00 Hs. (este ultimo servicio no sale los sabados)</li>
			<li><strong>Panamericano:</strong>sale a las 6.00 desde San Salvador de Jujuy, pasa por Purmamarca a las 7.30, por Tilcara a las 8.00 y a las 8.30 por Humahuaca para llegar a las 11.30 a Iruya.</li>
			</ul>
			<p><strong>Regreso desde Iruya:</strong></p>
			<ul><li><strong>Transporte Iruya</strong>: 6.00 (este primer servicio no sale los domingos) , 13.00 y 15.15 Hs.</li><li><strong>Panamericano</strong>: sale a las 14.00, va a san salvador de Jujuy, parando en Tilcara y Purmamarca.</li>
			</ul>
		</div>
		<p><strong>Para el acceso a este lugar, se puede hacer en vehículo  propio o en colectivo.</strong></p>
		<p>En Vehiculo propio o en colectivo existe una única forma de  hacerlo, pasando la provincia de Jujuy, recorriendo la ruta Nacional N&ordm; 9, a 26  km. De la capital de Humahuaca está el <strong>cruce  de rutas</strong>, donde se lee "<strong>Iruya  54 Km</strong>". Siguiendo el camino indicado, se llega a la estación Iturbe Provincia  de Jujuy, distante a 8 km. del cruce mencionado. Desde allí siempre por camino  de tierra, se llega al paraje denominado <strong>"ABRA  DEL CONDOR" a 4000 msnm</strong>, límite de la provincia de Salta y Jujuy.</p>
		<p>En el Abra del Cóndor comienza el descenso de 1220 m. En 19  Km (distancia del Abra a Iruya). <br /> A lo largo de estos 19 Km, el visitante se sumerge en la  variedad más insólita de colores, que van del verde agreste al morado o  violeta, pasando por el amarillo y el azul metálico. La montaña, en conjunto  con las quebradas, ofrece a la vista, caprichosas y curiosas formas que se  desdibujan en el lecho del río Colanzulí, a cuya vera corre el camino.</p>
		<p>Al llegar a Iruya, la primera impresión es de un pueblo <strong>"colgado en la montaña"</strong>, o  más bien, de una isla, ya que está rodeado por los ríos Colanzulí -o Iruya- y  Milmahuasi.</p>
		<p>Se destaca su edificación colonial con callejuelas estrechas  y sus paisajes de imponentes vistas panorámicas. En sus proximidades se  encuentran las ruinas del (se pronuncia en la zona como pucara y no  "pucará") pucara de Titiconte. Y las distintas comunidades que se  encuentran a en sus alrededores como: <strong>San  Isidro, San Juan, Chiyayoc, Rodeo Colorado, etc.</strong></p>
		<p>Aquí, los habitantes, vestimentas, costumbres y viviendas  han mantenido su tradición a lo largo de 250 años. El poblado conserva sus  calles angostas y empedradas, con casa de adobes, piedras y paja.</p>
		<p>El <strong>camino carretero</strong> termina, solamente a lomo de mula es posible realizar un viaje al interior del  departamento, donde se presenta el paisaje montañoso en toda su agresividad y  magnitud. Siguiendo el lecho de los que surcan el interior, se aprecian  quebradas de diferentes formas y colores. La piedra laja, en algunos tramos ha  formando paredes de contención del río</p>
	</section>
	<section class="columns small-12 medium-4 large-4">
		<!-- IruyaOnline -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-2926555334181048"
		     data-ad-slot="6267303263"
		     data-ad-format="auto"></ins>
		<ul class="small-block-grid-2 medium-block-grid-2 large-block-grid-2 clearing-thumbs" data-clearing>
			<li><a class="th" title="Camino a Iruya" href="//d3bgb83gsu3958.cloudfront.net/imagenes/abra008.jpg"><img src="m-abra008.jpg" alt="Camino a Iruya" /></a></li>
			<li><a class="th" title="Camino a colanzuli - Iruya" href="//d3bgb83gsu3958.cloudfront.net/imagenes/008.jpg"><img src="m-008.jpg" alt="Camino a colanzuli - Iruya" /></a></li>
			<li><a class="th" title="Bajando desde el abra del Condor" href="//d3bgb83gsu3958.cloudfront.net/imagenes/abra001.jpg"><img src="m-abra001.jpg" alt="Bajando desde el abra del Condor" /></a></li>
			<li><a class="th" title="Campo carreras de Iruya" href="//d3bgb83gsu3958.cloudfront.net/imagenes/consejo09.jpg"><img src="m-consejo09.jpg" alt="Campo carreras de Iruya" /></a></li>
		</ul>
	</section>
</content>
