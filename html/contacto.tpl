<title>Contacto</title>
<description>Iruya, esta a los 2.780 m.s.n.m. (metros sobre el nivel del mar), aproximadamente a una distancia de 320Km. de la capital Salteña.</description>
<content>
	<section class="columns small-12 medium-12 large-12">
		<h1>Contacto</h1>
		<form action="enviarmail.php" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="small-12 large-6 columns">
				<label>Nombre Completo <input type="text" placeholder="" name="nombre" /> </label>
				<label>Email <input type="text" placeholder="Ingrese su email, para recibir la respuesta a su consulta" name="email" /> </label>
				<label>Asunto <input type="text" placeholder="Especifique el motivo de su consulta." name="asunto" /> </label>
			</div>
			<div class="small-12 large-4 columns">
				<h5>Datos:</h5>
				<p><a href="">info@iruyaonline.com</a> </p>
				<p>Dirección: San Martin s/n - Iruya - Salta</p>

			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<label>Mensaje
					<textarea placeholder="Escriba aqui su consulta: " style="height: 100px" name="mensaje"></textarea>
				</label>
			</div>
			<div class="large-12 columns text-center">
				<input type="submit" value="Enviar" class="button">
			</div>
		</div>
		</form>
	</section>
</content>