<title>Fiesta del Rosario</title>
<description>Provee a sus clientes de un ambiente confortable y acogedor con una atención personalizada y cordial. Donde se brinda una gran variedad de comidas tanto regionales como minutas.</description>
<content>
	<section class="columns small-12 medium-8 large-8">
		<h1>Fiesta patronal en honor a Nuestra Señora del Rosario</h1>
		<p>Se aproxima la fiesta y los Iruyanos, productores, comerciantes y peregrinos de una gran cantidad de otros distritos, ya preparándose.<br>
		El primer domingo de octubre de todos los años en Iruya, se festeja la fiesta patronal del Pueblo, en la cual asisten una innumerable cantidad de personas, que año tras año llega para honrar a Nuestra Señora del Rosario.</p>
		<p>La fiesta comienza con la novena, el 26 de setiembre de este año 2008, continuando en las vísperas de la fiesta a partir del día viernes 3 con la llegada de los distintos productores y comerciantes de distintos lugares tanto como de la puna jujeña y los valles Salteños.</p>
		<p>El Sábado 4 por la mañana, se celebra la misa con la primera procesión de la Virgen con el acompañamiento de la gran cantidad de imágenes religiosas que llegan de las comunidades del interior y la adoración incansable de Cachis, mientras el sacerdote y el pueblo entonan el aleluya.</p>
		<p>Ya en la tarde de este día, se inicia la misa y la procesión nocturna que se festeja con una gran cantidad de fuegos artificiales y una nueva adoración de los cachis, al son permanente de cornetas, la quena y la caja con el repique de campanas, para terminar con la procesión se culmina la tradicional Luminaria de Víspera de la Fiesta.</p>
		<p>	Y al finalizar la gran serenata a la virgen, donde participan artistas locales en representación de la cultura del lugar y artistas invitados y los que se llegan a honrar a la Virgen.</p>
		<p>El Domingo 5, tempranito al amanecer suenan las salvas de bomba, anunciando la el inicio de la festividad, y con una primera misa a la mañana. </p><p>
		A media mañana, la misa solemne, que agrupa a cientos de peregrinos y promesantes. Al culminar la misa, el obispo encabeza junto al cura párroco, una larga procesión que recorre todo el pueblo, bajo la permanente e ininterrumpida adoración de los cachis. Finalizando la ceremonia con el retiro de las imágenes, y la despedida a Nuestra Señora del Rosario.</p>
		<p>Por la tarde del mismo día (mas precisamente en la noche) el esperado Baile Popular de los jóvenes y la gran mayoría de la gente que visita en esos días a Iruya.</p>
		<h3>La Feria y el Trueque</h3>
		<p>Como ya había mencionado anteriormente, productores y negociantes de distintos parajes y comunidades del interior de Iruya, la puna jujeña y los valles salteños, llegan en vísperas de la fiesta y se quedan hasta el día después de la fiesta.</p>
		<p>Durante estos días en la playa (la feria), los productos de cada unos son expuesto y negociados, Es así el maíz, las papas, “despapaos”, cebada, miel de abejas, platos de madera y una innumerable cantidad de productos que son producidos en la zona de los valles son canjeados por charqui, chalona, lanas de llamas y oveja etc., productos de la Puna. Culminándose así la “Fiesta del Rosario”.</p>
		<h3>Octava de la Fiesta.</h3>
		<p>El Segundo fin de semana comenzando el día sábado se comienza a media mañana con el Repique de Campanas, el ángelus y la adoración de los cachis. Y por la tarde se da el rezo del Santo Rosario, seguidamente la Santa Misa, la procesión y adoración de los cachis, luminaria y Fuegos Artificiales.<br>
		Y al finalizar, se da el segundo Concurso del Tamal.<p>
		<p>La Mañana Siguiente, se inicia el día, también bien temprano con Salva de Bombas, Adoración del Alba por los Cachis. Comenzando a media mañana la ceremonia con la Misa, Procesión y Adoración de los Cachis. En este día se celebra uno de los actos muy que solo se da una vez al año, la adoran los Cachis chiquitos y la capada del torito (cachi).</p>
		<ul class="clearing-thumbs small-block-grid-2 medium-block-grid-4 large-block-grid-4" data-clearing>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/fiesta_rosario.jpg" title="Fiesta del Rosario"><img src="s-fiesta_rosario.jpg" alt="fiesta rosario"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/cachis_chiquitos.jpg" title="cachis chiquitos"><img src="s-cachis_chiquitos.jpg" alt="cachis chiquitos"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/cachis_chiquitos2.jpg" title="cachis chiquitos"><img src="s-cachis_chiquitos2.jpg" alt="cachis chiquitos"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/capada_torito.jpg" title="Capada de Torito"><img src="s-capada_torito.jpg" alt="capada torito"></a></li>
		</ul>
	</section>
	<section class="columns small-12 medium-4 large-4">
		<!-- IruyaOnline -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-2926555334181048"
		     data-ad-slot="6267303263"
		     data-ad-format="auto"></ins>
		<ul class="clearing-thumbs small-block-grid-2 medium-block-grid-2 large-block-grid-2 "data-clearing>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/chachis_iruya.jpg" title="Chachis iruya"><img src="s-chachis_iruya.jpg" alt="chachis iruya"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/fiesta_patronal.jpg" title="Fiesta patronal - procesion"><img src="s-fiesta_patronal.jpg" alt="fiesta patronal"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/Fiesta_del_Rosario.jpg" title="Fiesta del Rosario"><img src="s-Fiesta_del_Rosario.jpg" alt="Fiesta del Rosario"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/Fiesta_del_Rosario3.jpg" title="Fiesta del Rosario"><img src="s-Fiesta_del_Rosario3.jpg" alt="Fiesta del Rosario"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/Fiesta_del_Rosario2.jpg" title="Fiesta del Rosario"><img src="s-Fiesta_del_Rosario2.jpg" alt="Fiesta del Rosario"></a></li>
		</ul>
	</section>
</content>