<title>Historia de Iruya</title>
<description>El Pueblo de Iruya Propiamente, fue fundado en el año 1753, sin embargo su origen remota a un siglo anterior a su fecha de fundación, actas de nacimiento encontrados en la parroquia de Humahuaca en la provincia de Jujuy, testifican que un siglo antes de su fundación ya estaban asentados habitantes en el lugar.</description>
<content>
	<section class="columns small-12 medium-8 large-8">
		<h1 class="title">Historia de Iruya</h1>

		<p>El Pueblo de Iruya Propiamente, fue fundado  en el año 1753, sin embargo su origen remota a un siglo anterior a su fecha de  fundación, actas de nacimiento encontrados en la parroquia de Humahuaca en la  provincia de Jujuy, testifican que un siglo antes de su fundación ya estaban  asentados habitantes en el lugar.</p>
		<p>Estos asentamientos son sobre todo  asentamientos indígenas cuyos antecedentes más remotos son los ocloyas, un  pueblo perteneciente a la etnia kolla, quienes a su vez, derivan del kollasuyo,  una de las cuatro regiones del antiguo Tahuantinsuyo (imperio  incaico).</p>
		<p>Numerosas Ruinas (antígales) dispersas por  los alrededores prueban la existencia de una etnia más o menos homogénea antes  de la llegada de los españoles; un ejemplo ejemplo claro de ellos son la ruinas  de Titicónte, ubicado al este de iruya a unos 8kms.</p>
		<p>Los primeros habitantes practicaban la  agricultura y la ganadería ambas en muy baja escala, ya que solo les permitía  la subsistencia de sus pobladores, cultivaban maíz, papas, ocas y otros  productos agrícolas; al mismo tiempo criaban ovejas, cabras y llamas, aunque  esta última en menor medida. En nuestros días ha disminuido la cría de ovejas y  cabras, sin embargo se ha extinguido la de llamas.</p>
		<h3>El Trueque en el pueblo de Iruya</h3>

		<p>Para los Pueblos Aborígenes del Imperio  incaico, el trueque fue una de las económicas, mas significantes para la  supervivencia humana, tal es el caso que para esto se fomentara, los incas se  organizaron en todo su territorio en comunidades. Cada una de estas una función  económica. </p>
		<p>Por ejemplo una comunidad producía papa, otras fruta, ganados y así  sucesivamente, esta organización les permitía un cambio reciproco de productos  y de esta manera estas etnias podían abastecerse y solventar las demandas de productos  agrícolas y así lograr la subsistencia.</p>
		<p>Tal como ocurrió en la antigüedad, el trueque  en Iruya es una de las actividades comerciales más tradicionales que se  realizan en la actualidad para solventar las necesidades de vida de la  población.</p>
	</section>
	<section class="columns small-12 medium-4 large-4">
		<!-- IruyaOnline -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-2926555334181048"
		     data-ad-slot="6267303263"
		     data-ad-format="auto"></ins>
		<ul class="small-block-grid-2 medium-block-grid-2 large-block-grid-2 clearing-thumbs" data-clearing>
			<li><a class="th" title="Cultivos de papa y otras en los cerros" href="//d3bgb83gsu3958.cloudfront.net/imagenes/araguyoc066.jpg"><img src="m-araguyoc066.jpg" alt="Cultivos de papa y otras en los cerros"></a></li>
			<li><a class="th" title="Ruinas en los alrrededores de Iruya" href="//d3bgb83gsu3958.cloudfront.net/imagenes/araguyoc011.jpg"><img src="m-araguyoc011.jpg" alt="Ruinas en los alrrededore de Iruya"></a></li>
			<li><a class="th" title="Vasijas" href="//d3bgb83gsu3958.cloudfront.net/imagenes/075.jpg"><img src="m-075.jpg" alt="vasijas"></a></li>
			<li><a class="th" title="Frutas de la Zona" href="//d3bgb83gsu3958.cloudfront.net/imagenes/011.jpg"><img src="m-011.jpg" alt="Frutas de la Zona"></a></li>
		</ul>
	</section>
</content>
