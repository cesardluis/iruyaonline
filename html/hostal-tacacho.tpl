<title>Hostal Tacacho, Hosteria de Iruya</title>
<description>En el mágico pueblo de Iruya, inmerso entre montañas y quebradas, se encuentra Hostal Tacacho</description>
<banner>
	<li><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/hostal_tacacho_01.jpg" alt="Hostal Tacacho"></li>
	<li><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/hostal_tacacho_02.jpg" alt="Hostal Tacacho"></li>
	<li class="active"><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/hostal_tacacho_03.jpg" alt="Hostal Tacacho"></li>
	<li><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/hostal_tacacho_04.jpg" alt="Hostal Tacacho"></li>
	<li><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/hostal_tacacho_05.jpg" alt="Hostal Tacacho"></li>
</banner>
<content>
	<section class="columns small-12 medium-8 large-8">
		<h1>Hostal Tacacho - Hostal de Iruya</h1>
		<p>En el mágico pueblo de Iruya, inmerso entre montañas y quebradas, se encuentra Hostal Tacacho. Atendido por sus dueños, nuestra propuesta es hacer de su estadía una experiencia inolvidable combinando el descanso con el encanto de nuestra cocina típica.</p>
		<strong>Habitaciones</strong>
		<p>Disponemos de amplias habitaciones, dobles, triples con baño privado y con vistas a los cerros.</p>
		<strong>Servicios en Habitaciones:</strong>
		<ul>
			<li>Ropa blanca</li>
			<li>Amenities</li>
			<li>Calefacción panel eco sol</li>
			<li>TV</li>
		</ul>
		<strong>Servicios:</strong>
		<ul>
			<li>Restaurante de comidas típicas</li>
			<li>Terraza con vista a los cerros</li>
			<li>Internet</li>
			<li>Información turística</li>
			<li>Excursiones</li>
		</ul>
		<strong>Restaurant: (solo funciona en temporada alta.)</strong>
		<p>Nuestra cocina de autor es reconocida por quienes quieren disfrutar de los sabores andinos:</p>
		<ul>
			<li>Tortillas de Quínoa</li>
			<li>Cazuela de Cabrito</li>
			<li>lomo de llama</li>
			<li>Empanadas</li>
			<li>Humitas</li>
		</ul>
		<ul class="pricing-table">
			<li class="price">Reserva</li>
			<li class="bullet-item text-left">
				<p>Para efectuar la reserva, deberá  gestionarla a traves del Sistema de Reserva Online (<strong>Recomendable</strong>)</p>
				<p>Todo haciendo clic en el siguiente boton "Haga su Reserva Aquí":</p>
			</li>
			<li class="bullet-item">
				<a class="highframe" title="Reserva Online" href="http://reservas.pueblonorte.com/reserva.php?a=NA=="><img class="img" title="Reserva Online" src="imagenes/reserva-online.jpg" alt="Reserva Online"></a><br>
				<p>
					Plaza La Tablada s/n, Iruya, Salta – Argentina<br> (0387) 154 195407 <br>En caso que no pueda comunicarse, por favor deje un mensaje de texto y el Hostal le devolvera su llamada<br> <a href="http://www.iruyahostaltacacho.com/" target="_blank">www.iruyahostaltacacho.com</a>
				</p>
			</li>
		</ul>
	</section>
	<section class="columns small-12 medium-4 large-4">
		<ul class="pricing-table">
			<li class="title"><a href="http://www.iruyahostaltacacho.com/" target="_blank">www.iruyahostaltacacho.com</a></li>
			<li class="description">
				<!-- Recomendado -->
				<div id="TA_rated712" class="TA_rated">
					<ul id="eYA0uBYpIj8" class="TA_links qmYR04Pi">
						<li id="wr1Cz8KwG" class="DldHHHia1U">
							<a target="_blank" href="http://www.tripadvisor.com.ar/"><img src="http://www.tripadvisor.com.ar/img/cdsi/img2/badges/recommended_es-11424-2.gif" alt="TripAdvisor"/></a>
						</li>
					</ul>
				</div>
				<script src="http://www.jscache.com/wejs?wtype=rated&amp;uniq=712&amp;locationId=2568916&amp;lang=es_AR"></script>
			</li>
			<li class="description show-for-medium-up">
				<!-- Comentario -->
				<div id="TA_selfserveprop960" class="TA_selfserveprop">
					<ul id="NtVT1SditEam" class="TA_links AUCRQxm7">
						<li id="CgsYH6r" class="7SiWGvCWL"><a target="_blank" href="http://www.tripadvisor.com.ar/"><img src="http://www.tripadvisor.com.ar/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a></li>
					</ul>
				</div>
				<script src="http://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=960&amp;locationId=2568916&amp;lang=es_AR&amp;rating=true&amp;nreviews=5&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true"></script>
			</li>
		</ul>
		<!--<ul class="pricing-table">
			<li class="description">
				<a href="imagenes/hostal-tacacho-mapa.jpg" target="_blank">
					<img class="show-for-medium-up" src="imagenes/hostal-tacacho-mapa.jpg" alt="Mapa del hospedje">
					<span class="show-for-small-only">Mapa del hospedje</span>
				</a>
			</li>
		</ul>-->
	</section>
</content>
