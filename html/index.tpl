<title>Iruya - Salta (información turística y excursiones)</title>
<description>Iruya, esta a los 2.780 m.s.n.m. (metros sobre el nivel del mar), aproximadamente a una distancia de 320Km. de la capital Salteña</description>
<banner></banner>
<content>
	<section class="columns small-12">
		<h1>Iruya - Salta</h1>
			<ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-2">
				<li><a title="Milmahuasi Hostel" href="milmahuasi-hostel.html"><img class="img" src="//d3bgb83gsu3958.cloudfront.net/imagenes/banner-milmahuasi.jpg" alt="Milmahuasi Hostel"></a></li>
				<li><a title="Hostal tacacho" href="hostal-tacacho.html"><img class="img" src="//d3bgb83gsu3958.cloudfront.net/imagenes/banner-tacacho.jpg" alt="Hostal Tacacho"></a></li>
			</ul>
			<!-- IruyaOnline -->
			<ins class="adsbygoogle"
				style="display:block"
				data-ad-client="ca-pub-2926555334181048"
				data-ad-slot="6267303263"
				data-ad-format="auto"></ins>
			<p>Iruya, esta a los 2.780 m.s.n.m. (metros sobre el nivel del mar), aproximadamente a una distancia de 320Km. de la capital Salteña.</p>
			<p>Es conocido hasta ahora por su belleza geográfica y su pueblo de “ensueños”. Sin embargo el departamento goza de múltiples identidades culturales, económicas y políticas (tradiciones, costumbres de vida, formas de organización, etc.). </p>
			<p>Aquí en el pueblo de Iruya la cultura aborigen se entrecruza con la cultura hispana, logrando la supervivencia de ambas, lo cual ha generado un proceso histórico de interculturalidad.</p>
			<p>Iruya, su nombre tiene una variedad de significados, y de acuerdo a la antigüedad del pueblo a pesar de ser un pueblo relativamente nuevo con respecto a las comunidades del Interior. Se detallan a continuación dos de sus significados ya que se acercan más al origen de la palabra “iruya”:</p>

			<p>IRUYA: Voz quechua o aymara, proveniente de la palabra IRUYOC “Iru” = paja y “yoc” = abundancia, iruya = abundante paja.</p>
			<ul class="clearing-thumbs small-block-grid-2 medium-block-grid-2 large-block-grid-4"  data-clearing>
				<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/fotos085.jpg" title="Llegada a Iruya"><img alt="Arrival Iruya" src="m-fotos085.jpg" class="img"></a></li>
				<li><a class="th" title="Iruya desde arriba" href="//d3bgb83gsu3958.cloudfront.net/imagenes/iruya-de-arriba.jpg"><img class="img" src="m-iruya-de-arriba.jpg" alt="Iruya desde arriba"></a></li>
				<li><a class="th" title="Plaza la Tablada" href="//d3bgb83gsu3958.cloudfront.net/imagenes/DSC01458.jpg"><img class="img" src="m-DSC01458.jpg" alt="plaza la tablada"></a> </li>
				<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/octava037.jpg" title="Cachis de Iruya"><img alt="Cachis de Iruya" src="m-octava037.jpg" class="img"></a></li>
			</ul>
			<div class="panel">
				<div class="fb-comments" data-href="http://www.iruyaonline.com" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
			</div>
	</section>
</content>