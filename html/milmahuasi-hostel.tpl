<title>Milmahuasi Hostel</title>
<description>Milmahuasi Hostel, cuenta con habitaciones dobles y amplios dormitorios con baño privado y agua caliente, tenemos sabanas de algodón,  edredones (colchas de plumas), frazadas y colchas artesanales</description>
<banner>
	<li><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/milmahuasi_hostel_01.jpg" alt="Milmahuasi Hostel"></li>
	<li><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/milmahuasi_hostel_02.jpg" alt="Milmahuasi Hostel"></li>
	<li class="active"><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/milmahuasi_hostel_03.jpg" alt="Milmahuasi Hostel"></li>
	<li><img src="//d3bgb83gsu3958.cloudfront.net/imagenes/milmahuasi_hostel_04.jpg" alt="Milmahuasi Hostel"></li>
</banner>
<content>
	<section class="columns small-12 medium-8 large-8">
		<h1>Hotel en Iruya: Milmahuasi Hostel</h1>
		<p style="clear:left">Milmahuasi Hostel, cuenta con  habitaciones dobles y amplios dormitorios con baño privado y agua caliente, tenemos sabanas de algodón, edredones (colchas de plumas), frazadas y  colchas artesanales. Posee terraza con vista a todo el esplendor del río  Milmahuasi. Milmahuasi Hostel es parte de una propuesta integral de desarrollo  del ecoturismo cultural con responsabilidad social (con inclusión de la  comunidad local). Aquí encontrara toda la información para visitar comunidades  indígenas vecinas, además de la historia, cultura, geología y problemática  socioeconómica local.Ubicado a 150 m de la Iglesia de Iruya. Desde  Humahuaca son 3 horas de viajes por un camino de montaña, pasando por un abra a  4000 metros, se puede llegar en auto particular con chofer con experiencia en  caminos de montaña, o si no en bus de línea, sale desde Humahuaca a las 8.30,  10.30 y 15.00 Hs. Iruya es un pueblo pequeño con solo 1200  habitantes, con mayoría de origen indígena Kolla.</p>

		<h3>Quiénes somos</h3>
		<p><strong>Dora:</strong> Prepara las cenas vegetarianas (con posibilidad de algunas excepciones) Tarta de verduras acompañadas de papas andinas y ocas al horno, ensalda y arroz, ademas de una deliciosa Sopa de verduras casera. Tambien colabora con la administracion del Hostal. En sus ratos libres da talleres de pintura para los jovenes y niños de Iruya.</p>
		<p><strong>Gladys:</strong> colabora con la limpieza del Hostal, vecina de Iruya, mama de 4 niños y abuela de 2 nietos.</p>
		<p><strong>Victor Bretscher:</strong> geólogo, asesor de ecoturismo en áreas indígenas (fue el asesor tecnico del proyecto que financio el Banco Mundial para el desarrollo del Ecoturismo en la comunidad indigena de Finca Potrero, San Isidro, Iruya, designado por la propia comunidad), sus últimos años los repartió entre Zurich, Suiza e Iruya. Colabora en diferentes proyectos de desarrollo del ecoturismo en áreas indígenas. Viajero, en especial de America del sur, es el coordinador de esta propuesta. </p>
		<p><strong>Edmundo Diaz:</strong> coordinador de los agentes sanitarios del hospital de Iruya, oriundo de San Isidro. Juntos con su Sra. Nélida y su familia de 7 hijos fueron construyendo el Hostal Milmahuasi. Hoy comenzamos esta nueva etapa con un emprendimiento de turismo donde locales y foráneos trabajamos con un mismo objetivo, el desarrollo armónico del turismo cultural con inclusión social en Iruya, evitando que se repitan modelos de desarrollo turístico no sustentables y sin participación local. </p>

		<h3>Habitaciones dobles matrimoniales</h3>
		<p>Cuentan con Somier, sabanas de algodon 100%, 180 hilos percal, edredones  (acolchados de plumas), frazadas y colchas artesanales. Baño privado, agua  caliente, y vista al rio Milmahuasi, </p>
		<h3>Habitaciones; dobles, triples,  cuadruples, o dormitorios a compartir</h3>
		<p>Cuentan con camas de una plaza con sabanas de algodon, <strong>edredones (acolchados de plumas), frazadas y colchas  artesanales. </strong><strong>Baño en la habitacion, agua caliente.</strong></p>

		<ul class="pricing-table">
			<li class="bullet-item">Para efectuar la reserva, deberá  gestionarla a traves del Sistema de Reserva Online (<strong>Recomendable</strong>)</li>
			<li class="bullet-item">
				Victor Bretscher <br /> 387 154 457 994<br />
				<a href="mailto:milmahuasi@gmail.com"> milmahuasi@gmail.com </a><br />web: <a href="http://www.milmahuasi.com/" target="_blank">www.milmahuasi.com</a> <br />
				<strong>Ubicación: </strong> A 150 m de la Iglesia de Iruya- Calle Salta s/n - Iruya - Salta - Argentina
			</li>
		</ul>
		<ul class="small-block-grid-2 medium-block-grid-2 large-block-grid-4 clearing-thumbs" data-clearing>
			<li><a class="th" title="Habitacion Doble" href="//d3bgb83gsu3958.cloudfront.net/imagenes/habitacion-doble-1.jpg"><img class="img" src="m-habitacion-doble-1.jpg" alt="habitacion-doble-1.jpg" /></a></li>
			<li><a class="th" title="Domitorio" href="//d3bgb83gsu3958.cloudfront.net/imagenes/dormi-2.jpg"><img class="img" src="m-dormi-2.jpg" alt="dormi-2.jpg" /></a></li>
			<li><a class="th" title="Pasillo" href="//d3bgb83gsu3958.cloudfront.net/imagenes/pasillo.jpg"><img class="img" src="m-pasillo.jpg" alt="pasillo.jpg" /></a></li>
			<li><a class="th" title="Baño" href="//d3bgb83gsu3958.cloudfront.net/imagenes/banio-2.jpg"><img class="img" src="m-banio-2.jpg" alt="banio-2.jpg" /></a></li>
		</ul>
	</section>
	<section class="columns small-12 medium-4 large-4">
		<ul class="pricing-table">
			<li class="title"><a href="http://www.milmahuasi.com/" target="_blank">www.milmahuasi.com</a> </li>
			<li class="bullet-item">Consultar disponibilidades para las fechas que Ud seleccione en el momento.</li>
			<li class="bullet-item">Consultar precios Actualizados.</li>
			<li class="bullet-item">Iniciar una reserva con unos simples pasos.</li>
			<li class="bullet-item"><a title="Reserva Online" href="http://reservas.pueblonorte.com/reserva.php?a=Ng==" target="_blank"><img class="img" title="Reserva Online" src="//d3bgb83gsu3958.cloudfront.net/imagenes/reserva-online.jpg" alt="Reserva Online"></a></li>
		</ul>

		<ul class="pricing-table">
			<li class="descripcion text-center"><a title="Ubicacion" href="//d3bgb83gsu3958.cloudfront.net/imagenes/milmahuasi-hostel-mapa.jpg"><img class="img" src="m-milmahuasi-hostel-mapa.jpg" alt="milmahuasi hostel mapa" /></a></li>
			<li class="description">
				<!-- Recomendado -->
				<div id="TA_rated430" class="TA_rated"><ul id="I6pJM0P1x" class="TA_links HG28uGeVNw"><li id="I71tJXH4" class="at93uNz2qZyw"><a href=http://www.tripadvisor.com.ar/Hotel_Review-g982715-d1956533-Reviews-Milmahuasi_Hostel-Iruya_Province_of_Salta_Northern_Argentina.html>Milmahuasi Hostel</a></li></ul></div>
				<script src="http://www.jscache.com/wejs?wtype=rated&uniq=430&locationId=1956533&lang=es_AR"></script>
			</li>
			<li class="description">
				<!-- Certificado -->
				<div id="TA_certificateOfExcellence714" class="TA_certificateOfExcellence">
				<ul id="qYZV3mC4" class="TA_links Iqwm4X"><li id="Q6etXrIKvO" class="hRjNSH7"><a href=http://www.tripadvisor.es/Hotel_Review-g982715-d1956533-Reviews-Milmahuasi_Hostel-Iruya_Province_of_Salta_Northern_Argentina.html>Milmahuasi Hostel</a></li></ul></div>
				<script src="http://www.jscache.com/wejs?wtype=certificateOfExcellence&uniq=714&locationId=1956533&lang=es&year=2012"></script>
			</li>

			<li class="description show-for-medium-up">
				<!-- Comentario -->
				<div id="TA_selfserveprop323" class="TA_selfserveprop"><ul id="5PLvRn54noF" class="TA_links fVxHw2"><li id="xwzyrbTVhtq" class="6UfoUeHg5Q"><a target="_blank" href="http://www.tripadvisor.com.ar/Hotel_Review-g982715-d1956533-Reviews-Milmahuasi_Hostel-Iruya_Province_of_Salta_Northern_Argentina.html">Milmahuasi Hostel</a> en Iruya tiene 2 críticas</li></ul></div>
				<script src="http://www.jscache.com/wejs?wtype=selfserveprop&uniq=323&locationId=1956533&lang=es_AR&rating=true&nreviews=5&writereviewlink=true&popIdx=true&iswide=false&linkt=1"></script>
			</li>
		</ul>
		<!--<ul class="pricing-table">
			<li class="description">
				<a href="imagenes/hostal-tacacho-mapa.jpg" target="_blank">
					<img class="show-for-medium-up" src="imagenes/hostal-tacacho-mapa.jpg" alt="Mapa del hospedje">
					<span class="show-for-small-only">Mapa del hospedje</span>
				</a>
			</li>
		</ul>-->
	</section>
</content>
