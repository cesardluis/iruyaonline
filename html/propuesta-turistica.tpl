<title>Propuesta Turística en Iruya</title>
<description>Turismo Cultural: Iruya, además de los espectaculares paisajes y los pueblos cercanos más accesibles, tiene una gran cantidad lugares (y comunidades), a las que muy poco llega el turista y otros que son totalmente vírgenes.</description>
<content>
	<section class="columns small-12 medium-8 large-8">
		<h1 class="title">Propuesta Turística en Iruya</h1>
		<p>Proponemos a través de esta página y este emprendimiento  hacer un turismo diferente, un turismo que se salga de lo común conocido y lo  clásico. Dirigido a las personas interesadas en conocer la cultura de los  pueblos indígenas.</p>
		<p><strong>Turismo Cultural:</strong> Iruya, además de los espectaculares paisajes y los pueblos cercanos más  accesibles, tiene una gran cantidad lugares (y comunidades), a las que muy poco  llega el turista y otros que son totalmente vírgenes. Están ahí, con una  cultura casi intacta, con la gente kolla con su vestimenta de siempre,  cumpliendo cada uno todo los días sus trabajos, en los campos y en el hogar;  con los sembrados, los ganados, las artesanías y todas sus rutinas diarias. Por  otro lado diferentes relieves, vegetaciones y fauna, decoran el paisaje de esta  región.</p>
		<p><strong>Intercambio de Culturas:</strong> Nos basamos principalmente en servicios  que brindan la posibilidad del intercambio de culturas, visitando a comunidades  de este departamento. Hospedándonos en casa de familias, conociendo y viviendo  las actividades cotidianas de cada una y dando la posibilidad de que el  visitante pueda enriquecerse de esta cultura; como así la familia que lo reciba  se beneficie de una forma talque culturalmente se sienta orgullosa de su propia  cultura, y patrimonios, tanto como materiales como de identidad cultural, a su  vez que esta se beneficie económicamente.</p>
		<p><strong>Salir de las rutas  turísticas Clásicas: </strong>Mucha gente cuando llega por primera vez a Iruya, se  da la impresión de que Iruya es solo el pueblo y que no hay otra actividad más  que dar un paseo por el pueblo o visitar San Isidro, una comunidad que es la  más accesible. Pero en realidad como lo dije antes, en iruya se puede encontrar  una gran cantidad de atractivos, pueblos con una cultura precolombina casi sin  haber cambiado mucho, relieves que varían desde la puna has las selva, pasando  por valles angostos y muy hermosos. </p>
	</section>
	<section class="columns small-12 medium-4 large-4">
		<!-- IruyaOnline -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-2926555334181048"
		     data-ad-slot="6267303263"
		     data-ad-format="auto"></ins>
		<ul class="small-block-grid-2 medium-block-grid-2 large-block-grid-2 clearing-thumbs" data-clearing>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/campolargo.jpg" title="Campo Largo - Iruya"><img src="s-campolargo.jpg" alt="Campo largo -  iruya"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/piedelacuesta.jpg" title="Pie de la Cuesta - Iruya"><img src="s-piedelacuesta.jpg" alt="Pie de la cuesta - iruya"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/nubeiruya.jpg" title="Iruya - Vista a San juan"><img src="s-nubeiruya.jpg" alt="nubes-iruya"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/iglesiadeiruya.jpg" title="Iglesia de Iruya"><img src="s-iglesiadeiruya.jpg" alt="iglesia de iruya"></a></li>
			<li><a class="th" href="//d3bgb83gsu3958.cloudfront.net/imagenes/panoramicadeiruya.jpg" title="Vista panoramica de Iruya"><img src="s-panoramicadeiruya.jpg" alt="panoramica de iruya"></a></li>
		</ul>
	</section>
</content>
