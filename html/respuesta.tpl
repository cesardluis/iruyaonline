<title>Contacto</title>
<description>Iruya, esta a los 2.780 m.s.n.m. (metros sobre el nivel del mar), aproximadamente a una distancia de 320Km. de la capital Salteña.</description>
<content>
	<section class="columns small-12 medium-12 large-12">
		<h1>Contacto</h1>
		<p>Muchas gracias por escribirnos, en breve su consulta sera respondida.</p>
		<div class="small-12 large-4 columns">
			<h5>Datos:</h5>
			<p><a href="">info@iruyaonline.com</a> </p>
			<p>Dirección: San Martin s/n - Iruya - Salta</p>

		</div>
	</section>
</content>