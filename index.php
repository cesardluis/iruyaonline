<?php
header('Content-Type: text/html; charset=UTF-8');
include_once "libs/tmpl.class.php";

$s = !empty($_GET["s"]) ? $_GET["s"]: "index";

if($s == "iruya-comollegar"){
	header("Location: como-llegar.html",TRUE,301);
	exit;
}

$defaultBanner = file_get_contents("html/banner.tpl");

if (file_exists("html/".$s.".tpl")) {
	$pagina = file_get_contents("html/".$s.".tpl");

	preg_match("#<title>(.*)</title>#s", $pagina, $title);
	preg_match("#<description>(.*)</description>#s", $pagina, $description);
	preg_match("#<banner>(.*)</banner>#s", $pagina, $banner);
	preg_match("#<content>(.*)</content>#s", $pagina, $content);

	$data = array(
		"title"  => !empty($title[1]) ? $title[1]:"",
		"description"  => !empty($description[1]) ? $description[1]:"",
		"banner" => !empty($banner[1]) ? $banner[1]:$defaultBanner,
		"content"  => !empty($content[1]) ? $content[1]:"",
		"url" => $_SERVER["REQUEST_URI"]
	);
}else{
	header("HTTP/1.0 404 Not Found");
	$data = array(
		"title"  => "404 Not Found",
		"description"  => "",
		"banner" => "",
		"content"  => "<section class=\"columns small-12\" data-equalizer-watch><h2>Error 404: Página no encontrada.</h2>
		<p>No existe la página: ".$_SERVER["REQUEST_URI"].", por favor navega a través del menú.</p></section>",
		"url" => $_SERVER["REQUEST_URI"],
		"host" => "http://www.iruyaonline.com/"
	);
}
$html = new tmpl("template/iruyaonline.tpl", $data);
$html->display();




?>
