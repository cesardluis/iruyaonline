<?php 
/**
* simple mysql
*/
class MysqlDB{
	protected $_mysql;
	protected $_query;
	protected $_where;
	protected $_whereValue;

	public function __construct($host = MYSQL_HOST, $username = MYSQL_USER, $password = MYSQL_PASS, $db = MYSQL_NAME) {
		$this->_mysql = new mysqli($host, $username, $password, $db) or die('There was a problem connecting to the database');

		$this->_where = "";
		$this->_whereValue = "";
	}
	public function query($query, $numRows=0) {
		$result = $this->_mysql->query($query);
		//tipo de consulta
		$tipo = explode(" ",strtolower(trim($query)) );

		if ($tipo[0] == "select") {
			$resutado = array();
			while ($row = $result->fetch_assoc()) {
		    	$resutado[] = $row;
		    }
		    if($numRows == 1 && $result->num_rows > 0)
		    	$result = $resutado[0];
		    elseif ($result->num_rows == 0) 
		    	$result = 0;
		    else $result = $resutado;
		}
		return $result;
	}
	public function get($tableName, $numRows = 0) {

		$query = "SELECT * FROM ".$tableName;
		if ($this->_where != '')
			$query .= " WHERE ".$this->_where."= '".$this->_whereValue."'";
		
		$resutado = array();
		if ($result = $this->_mysql->query($query)) {
		    while ($row = $result->fetch_assoc()) {
		    	$resutado[] = $row;
		    }
		    $result->free();

		    if(count($resutado) > 0 && $numRows == 1)
				$resutado = $resutado[0];
		}
		return $resutado;
	}

	public function insert($tableName, $insertData) {
		$query = "INSERT INTO $tableName ".$this->_insertDataString($insertData);
		return $this->_mysql->query($query);
	}
	public function update($tableName, $insertData) {
		$query = "UPDATE $tableName SET ".$this->_insertDataString($insertData, "update")." WHERE ".$this->_where."='".$this->_whereValue."'";
		return $this->_mysql->query($query);
	}
	public function delete($tableName) {
		$query = "DELETE FROM $tableName  WHERE ".$this->_where."= '".$this->_whereValue."'";
		return $this->_mysql->query($query);

	}
	public function where($where="", $whereValue=""){
		$this->_where = $where;
		$this->_whereValue = $whereValue;
	}
	public function getId(){
		return $this->_mysql->insert_id;
	}

	private function _insertDataString($data, $type="insert"){
		if($type == "insert"){
			$campos = array();
			$valores = array();
			foreach ($data as $key => $value) {
				$campos[] = $key;
				if(is_string($value))
					$valores[] = "'".$this->_mysql->real_escape_string($value)."'";
				else
					$valores[] = $value;
			}
			return "(".implode(", ", $campos).") VALUES (".implode(", ", $valores).")";
		}

		if($type == "update"){
			$valores = array();
			foreach ($data as $key => $value) {
				$valores[] = $key."='".$this->_mysql->real_escape_string($value)."'";
			}
			return implode(", ", $valores);
		}
	}
	public function __destruct() {
		$this->_mysql->close();
	}
}




?>