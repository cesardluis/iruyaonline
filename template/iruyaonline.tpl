<!doctype html>
<html class="no-js" lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>[title]</title>
	<meta name="description" content="[description]" />
	<link rel="stylesheet" href="//d3bgb83gsu3958.cloudfront.net/css/foundation.min.css" />
	<link rel="stylesheet" href="//d3bgb83gsu3958.cloudfront.net/css/estilos.min.css" />
	<link rel="icon" href="favicon.ico" type="image/x-icon"/>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
	<meta name="google-site-verification" content="gnSbf1OhbmGT-f0cv9Yzh-UTGyM_ZdrJVz1cVAjTHqc" />
</head>
<body>
<header class="row">
		<div class="slideshow-wrapper">
			<div class="preloader"></div>
			<ul data-orbit data-options="animation: slide; pause_on_hover: false; animation_speed: 1000; navigation_arrows: false; bullets: false; circular: true; slide_number: false;">
				[banner]
			</ul>
		</div>
		<div class="contain-to-grid sticky">
			<nav class="top-bar" data-options="back_text: ;" data-topbar>
				<ul class="title-area">
					<li class="name"><a href="/" class="title">IruyaOnline</a></li>
					<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
				</ul>
				<div class="top-bar-section">
					<!-- Right Nav Section -->
					<ul class="right">
						<li><a href="como-llegar.html">Como llegar</a></li>
						<li class="has-dropdown">
							<a href="#">Donde Alojarse</a>
							<ul class="dropdown">
								<li><a href="milmahuasi-hostel.html">Milmahuasi Hostel</a></li>
								<li><a href="hostal-tacacho.html">Hostal Tacacho</a></li>
							</ul>
						</li>
						<li><a href="comedor-margarita.html">Donde Comer</a></li>
						<!--<li class="has-dropdown">
							<a href="#">Caminatas</a>
							<ul class="dropdown">
								<li><a href="#">San Juan por San Isidro</a></li>
								<li><a href="#">San Isidro & Panti-pampa</a></li>
								<li><a href="#">Rodeo Colorado</a></li>
							</ul>
						</li> -->
						<li class="has-dropdown">
							<a href="#">Mas Información</a>
							<ul class="dropdown">
								<li><a href="propuesta-turistica.html">Propuesta Turística</a></li>
								<li><a href="historia.html">Historia de Iruya</a></li>
								<li><a href="fiesta-del-rosario.html">Fiesta del Rosario</a></li>
								<li><a href="#">Mapa de Iruya</a></li>
							</ul>
						</li>
						<li><a href="contacto.html">Contacto</a></li>
					</ul>
					<!-- Left Nav Section -->
					<ul class="left">
						<li><a href="#">Fotos</a></li>
					</ul>
				</div>
			</nav>
		</div>
</header>
<section class="content row">
	<div class="right text-right columns small-12 medium-4 large-4">
		<div class="share_btn">
			<div class="fb-share-button" data-href="http://www.iruyaonline.com[url]" data-type="box_count"></div>
		</div>
		<div class="share_btn">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical" data-url="http://www.iruyaonline.com[url]" data-via="iruyaonline" data-lang="es" data-dnt="true">Twittear</a>
		</div>
		<div class="share_btn">
			<div class="g-plusone" data-size="tall"  data-href="http://www.iruyaonline.com[url]"></div>
		</div>
	</div>
	[content]
</section>
<footer>
	<div class="row">
		<div class="columns columns small-12 medium-6">
			<h5>Sobre IruyaOnline.com</h5>
			<a href=""><span class="footer-logo"></span></a>
			<p>IruyaOnline es un sitio web creado por iniciativa propia de Cesar Luis, con el objetivo de acercar al viajero un poco mas de información sobre <strong>Iruya</strong>.</p>
			<p>El sitio web no es de ninguna entidad publica, ni agencia de de viajes, se mantien a si misma a travez de sus patrocinadores: <a href="milmahuasi-hostel.html">Milmahuasi Hoste</a>,  <a href="hostal-tacacho.html" class="buttom">Hostal Tacacho</a> y Google Adsense.</p>
			<p class="copyright opacity">&copy; Cear Luis 2014.</p>
		</div>
		<div class="columns columns small-12 medium-3">
			<h5>Site map</h5>
			<ul>
				<li><a href="como-llegar.html">Como llegar</a></li>
				<li><a href="milmahuasi-hostel.html">Milmahuasi Hostel</a></li>
				<li><a href="hostal-tacacho.html">Hostal Tacacho</a></li>
				<li><a href="comedor-margarita.html">Donde Comer</a></li>
				<li><a href="propuesta-turistica.html">Propuesta Turística</a></li>
				<li><a href="historia.html">Historia de Iruya</a></li>
				<li><a href="fiesta-del-rosario.html">Fiesta del Rosario</a></li>
				<li><a href="#">Mapa de Iruya</a></li>
				<li><a href="#">Contacto</a></li>
				<li><a href="#">Fotos</a></li>
			</ul>
		</div>
		<div class="columns columns small-12 medium-3">
			<h5>Contacto</h5>
			<p>Cesar Luis
				<br>San Martin s/n
				<br>Iruya - Salta
				<br>Argentina
				<a href="contacto.html">Email</a>
			</p>
		</div>
	</div>
</footer>
<script src="//d3bgb83gsu3958.cloudfront.net/js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//d3bgb83gsu3958.cloudfront.net/js/foundation.min.js"></script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script async src="//platform.twitter.com/widgets.js"></script>
<script type="text/javascript" src="//apis.google.com/js/platform.js"></script>
<script>
	$(document).foundation();
	$(document).foundation('clearing', 'init');
</script>

<div id="fb-root"></div>
<script>
	//facebook init
	$.getScript('http://connect.facebook.net/es_LA/sdk.js', function(){
		FB.init({appId: '251739988366765', xfbml: true, version: 'v2.0'});
	});
	//google ads
	(adsbygoogle = window.adsbygoogle || new Array()).push({});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1711517-4', 'auto');
  ga('send', 'pageview');
</script>
</body>
</html>